# Usa una imagen base de PHP con Apache
FROM php:8.2-apache

# Instala las dependencias necesarias
RUN apt-get update && apt-get install -y \
    curl \
    unzip \
    zip

# Instala Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Establece el directorio de trabajo
WORKDIR /var/www/html

# Copia el archivo de configuración y las dependencias de Composer
COPY composer.json composer.lock ./

# Instala las dependencias de Composer
RUN composer install --no-dev --optimize-autoloader --no-interaction

# Copia el resto de la aplicación
COPY . ./

# Establece los permisos correctos
RUN chown -R www-data:www-data /var/www/html

# Exponer el puerto 80 para el contenedor
EXPOSE 80
