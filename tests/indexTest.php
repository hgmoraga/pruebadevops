<?php

require __DIR__ . '/../vendor/autoload.php';
use PHPUnit\Framework\TestCase;

class IndexTest extends TestCase
{
    public function testHelloWorld()
    {
        $environment = $_ENV['ENV'];

        ob_start();
        require __DIR__ . '/../index.php';
        $output = ob_get_clean();

        $this->assertEquals("Hola Mundo! desde el ambiente $environment.", $output);
    }
}